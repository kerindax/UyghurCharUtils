public class demo {

    public static void main(String[] args) {
        UyghurCharUtils utils = new UyghurCharUtils();
        String source = "سالام Java";

        String target1 = utils.Basic2Extend(source);//基本区 转换 扩展区
        String target2 = utils.Extend2Basic(target1);//基本区 转换 扩展区

        String target3 = utils.Basic2RExtend(source);//基本区 转换 扩展区
        String target4 = utils.RExtend2Basic(target3);//基本区 转换 扩展区
        String target5 = utils.BasicSyllable(source);//音节索引
        
        System.out.println(target1 + "\n");
        System.out.println(target2 + "\n");
        System.out.println(target3 + "\n");
        System.out.println(target4 + "\n");
        System.out.println(target5 + "\n");
    }
}