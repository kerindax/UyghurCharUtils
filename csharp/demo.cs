
namespace ConsoleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            UyghurCharUtils utils = new UyghurCharUtils();
            string source = "سالام C#";
            string target1 = utils.Basic2Extend(source);//基本区 转换 扩展区
            string target2 = utils.Extend2Basic(target1);//扩展区 转换 基本区

            string target3 = utils.Basic2RExtend(source);//基本区 转换 反向扩展区
            string target4 = utils.RExtend2Basic(target3);//反向扩展区 转换 基本区
           
            string target5 = utils.BasicSyllable(source);//音节索引

            Console.WriteLine(target1);
            Console.WriteLine(target2);
            Console.WriteLine(target3);
            Console.WriteLine(target4);
            Console.WriteLine(target5);
        }
    }
}
