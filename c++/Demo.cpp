﻿#include "UyghurCharUtils.hpp"
#include <iostream>
#include <string>

int main()
{
    UyghurCharUtils *utils = new UyghurCharUtils();
    wstring source = L"سالام C++";

    wstring target1 = utils->Basic2Extend(source);//基本区 转换 扩展区
    wstring target2 = utils->Extend2Basic(target1);//扩展区 转换 基本区

    wstring target3 = utils->Basic2RExtend(source);//基本区 转换 反向扩展区
    wstring target4 = utils->RExtend2Basic(target3);//反向扩展区 转换 基本区

    wstring target5 = utils->BasicSyllable(source);// 音节索引

    std::wcout << target1 + L"\n";
    std::wcout << target2 + L"\n";
    std::wcout << target3 + L"\n";
    std::wcout << target4 + L"\n";
    std::wcout << target5 + L"\n";

}
