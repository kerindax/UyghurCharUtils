# UyghurCharUtils

### 项目介绍
##### 维吾尔语，哈萨克语，柯尔克孜语基本区和扩展区转换函数类库
##### v1版本地址：https://gitee.com/kerindax/UyghurCharUtils/tree/v1.x/
##### v1版本支持语言：javascript，php，csharp，vb.net，mysql，java，golang

##### v2版本项目地址：https://gitee.com/kerindax/UyghurCharUtils/tree/v2/
##### v2版本支持语言：javascript，php，csharp，vb.net，java

##### v3版本项目地址：https://gitee.com/kerindax/UyghurCharUtils
##### v3版本支持语言：javascript，php，csharp，vb.net，java，golang，swift，python3，c++

### 贡献者
Kerindax，Sherer，Bulut，UyCode, Erbash
### 联系
1482152356@qq.com

### 使用说明

1. Basic2Extend(source){}       基本区 转换 扩展区
2. Extend2Basic(source){}       扩展区 转换 基本区
3. Basic2RExtend(source){}      基本区 转换 反向扩展区
4. RExtend2Basic(source){}      反向扩展区 转换 基本区
5. BasicSyllable(source){}      音节索引

##### 克隆仓库
`git clone git@gitee.com:kerindax/UyghurCharUtils.git`

##### 安装依赖
```html
已取消更新
```
<!-- `npm install uyghur-char-utils` -->

##### CDN 引入

```html
已取消更新
```
<!-- <script src="https://unpkg.com/uyghur-char-utils"></script> -->

### 例子

- nodejs

```js
  const UyghurCharUtils = require("./UyghurCharUtils.js");
  var utils = new UyghurCharUtils();
  var source = "سالام JS";

  var target1 = utils.Basic2Extend(source); //基本区 转换 扩展区
  var target2 = utils.Extend2Basic(target1); //扩展区 转换 基本区

  var target3 = utils.Basic2RExtend(source); //基本区 转换 反向扩展区
  var target4 = utils.RExtend2Basic(target3); //反向扩展区 转换 基本区
  var target5 = utils.BasicSyllable(source);// 音节索引

  console.log(target1);
  console.log(target2);
  console.log(target3);
  console.log(target4);
  console.log(target5);
```
- browser

```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>UyghurCharUtils</title>
  <script type="text/javascript" src="./UyghurCharUtils.js" charset="utf-8"></script>
</head>
<body>
  <script>

    var utils  = new UyghurCharUtils();
    var source = "سالام JS";
    var target1 = utils.Basic2Extend(source);//基本区 转换 扩展区
    var target2 = utils.Extend2Basic(target1);//扩展区 转换 基本区

    var target3 = utils.Basic2RExtend(source);//基本区 转换 反向扩展区
    var target4 = utils.RExtend2Basic(target3);//反向扩展区 转换 基本区
    var target5 = utils.BasicSyllable(source);// 音节索引

    console.log(target1);
    console.log(target2);
    console.log(target3);
    console.log(target4);
    console.log(target5);

  </script>
</body>
```
- php
```php
	header("Content-type: text/html; charset=utf-8");
	require_once "UyghurCharUtils.php";
  $utils = new UyghurCharUtils();
  $source = "سالام PHP";

  $target1 = $utils->Basic2Extend($source);//基本区 转换 扩展区
  $target2 = $utils->Extend2Basic($target1);//扩展区 转换 基本区

  $target3 = $utils->Basic2RExtend($source);//基本区 转换 反向扩展区
  $target4 = $utils->RExtend2Basic($target3);//反向扩展区 转换 基本区
	$target5 = $utils->BasicSyllable($source);//音节索引

	echo $target1."<br/>";
	echo $target2."<br/>";
	echo $target3."<br/>";
	echo $target4."<br/>";
	echo $target5."<br/>";
```
- c#
```c#
static void Main(string[] args)
{
  UyghurCharUtils utils = new UyghurCharUtils();
  string source = "سالام C#";
  string target1 = utils.Basic2Extend(source);//基本区 转换 扩展区
  string target2 = utils.Extend2Basic(target1);//扩展区 转换 基本区

  string target3 = utils.Basic2RExtend(source);//基本区 转换 反向扩展区
  string target4 = utils.RExtend2Basic(target3);//反向扩展区 转换 基本区
  string target5 = utils.BasicSyllable(source);//音节索引

  Console.WriteLine(target1);
  Console.WriteLine(target2);
  Console.WriteLine(target3);
  Console.WriteLine(target4);
  Console.WriteLine(target5);
}
```
- vb.net
```vb
Sub Main()
    Dim utils As New UyghurCharUtils
    Dim source As String = "سالام VB.NET"

    Dim target1 As String = utils.Basic2Extend(source) '基本区 转换 扩展区
    Dim target2 As String = utils.Extend2Basic(target1) '扩展区 转换 基本区

    Dim target3 As String = utils.Basic2RExtend(source) '基本区 转换 反向扩展区
    Dim target4 As String = utils.RExtend2Basic(target3) '反向扩展区 转换 基本区
    Dim target5 As String = utils.BasicSyllable(source) '音节索引

    Console.WriteLine(target1)
    Console.WriteLine(target2)
    Console.WriteLine(target3)
    Console.WriteLine(target4)
    Console.WriteLine(target5)
End Sub
```
- java
```java
public class demo {
    public static void main(String[] args) {
        UyghurCharUtils utils = new UyghurCharUtils();
        String source = "سالام Java";

        String target1 = utils.Basic2Extend(source);//基本区 转换 扩展区
        String target2 = utils.Extend2Basic(target1);//基本区 转换 扩展区

        String target3 = utils.Basic2RExtend(source);//基本区 转换 扩展区
        String target4 = utils.RExtend2Basic(target3);//基本区 转换 扩展区
        String target5 = utils.RExtend2Basic(target3);//音节索引

        System.out.println(target1 + "\n");
        System.out.println(target2 + "\n");
        System.out.println(target3 + "\n");
        System.out.println(target4 + "\n");
        System.out.println(target5 + "\n");
    }
}
```
- golang
```golang
package main

import (
	"fmt"
	"./UyghurCharUtils"
)

func main() {
    utils := UyghurCharUtils()
	source := "سالام go"
	utils.Basic2Extend(source)	//基本区 转换 扩展区

    target1 := utils.Basic2Extend(source)	//基本区 转换 扩展区
	target2 := utils.Extend2Basic(target1)	//扩展区 转换 基本区
	target3 := utils.Basic2RExtend(source)	//基本区 转换 反向扩展区
	target4 := utils.RExtend2Basic(target3)	//反向扩展区 转换 基本区
	target5 := utils.BasicSyllable(source)	//音节索引

	fmt.Printf("\n%s",target1)
	fmt.Printf("\n%s",target2)
	fmt.Printf("\n%s",target3)
	fmt.Printf("\n%s",target4)
	fmt.Printf("\n%s",target5)
}
```
- swift
```swift
import UIKit

class DemoController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
          let source = "سالام swift"
        print("source: \(source)")
        
        guard
            let BasicSyllable = UyghurCharUtils.Shared.Convert(source, type: .BasicSyllable),
            let Basic2Extend = UyghurCharUtils.Shared.Convert(source, type: .Basic2Extend),
            let Extend2Basic = UyghurCharUtils.Shared.Convert(Basic2Extend, type: .Extend2Basic),
            let Basic2RExtend = UyghurCharUtils.Shared.Convert(source, type: .Basic2RExtend),
            let RExtend2Basic = UyghurCharUtils.Shared.Convert(Basic2RExtend, type: .RExtend2Basic)
        else {
            print("转换失败")
            return
        }
        
        print("BasicSyllable: \(BasicSyllable)")
        print("Basic2Extend: \(Basic2Extend)")
        print("Extend2Basic: \(Extend2Basic)")
        print("Basic2RExtend: \(Basic2RExtend)")
        print("RExtend2Basic: \(RExtend2Basic)")
    }
}
```

- python3
```python
from UyghurCharUtils import UyghurCharUtils

utils  = UyghurCharUtils()
source = "سالام Python";

target1 = utils.Basic2Extend(source) #基本区 转换 扩展区
target2 = utils.Extend2Basic(target1) #扩展区 转换 基本区

target3 = utils.Basic2RExtend(source) #基本区 转换 反向扩展区
target4 = utils.RExtend2Basic(target3) #反向扩展区 转换 基本区
target5 = utils.BasicSyllable(source) #音节索引

print(target1)
print(target2)
print(target3)
print(target4)
print(target5)
```

- c++
```cpp
#include "UyghurCharUtils.hpp"
#include <iostream>
#include <regex>
#include <string>

int main()
{
    UyghurCharUtils *utils = new UyghurCharUtils();
    wstring source = L"سالام C++";

    wstring target1 = utils->Basic2Extend(source);//基本区 转换 扩展区
    wstring target2 = utils->Extend2Basic(target1);//扩展区 转换 基本区

    wstring target3 = utils->Basic2RExtend(source);//基本区 转换 反向扩展区
    wstring target4 = utils->RExtend2Basic(target3);//反向扩展区 转换 基本区
    wstring target5 = utils->BasicSyllable(source);// 音节索引

    std::wcout << target1 + L"\n";
    std::wcout << target2 + L"\n";
    std::wcout << target3 + L"\n";
    std::wcout << target4 + L"\n";
    std::wcout << target5 + L"\n";
```