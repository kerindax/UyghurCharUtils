import UIKit

class DemoController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // MARK: - 如何使用 UyghurCharUtils ？
        /**
         1. 导入 UyghurCharUtils.swift
         2. 导入 UyghurCharUtils.js
         3. 使用 UyghurCharUtils.swift 提供的以下方法
         
         /// 转换类型枚举
         internal enum ConvertType : String {
         
         case Basic2Extend
         
         case Extend2Basic
         
         case Basic2RExtend
         
         case RExtend2Basic
         
         case BasicSyllable
         }
         
         /// 便捷转换函数
         /// - Parameters:
         ///   - text: 原文本
         ///   - type: 转换类型
         /// - Returns: 转换结果
         internal mutating func Convert(_ text: String, type: ConvertType) -> String?
         
         /// 基本区 转 音节索引
         /// - Parameter text: 基本区文本
         /// - Returns: 音节索引文本
         internal mutating func BasicSyllable(_ text: String) -> String?
         
         /// 基本区 转换 扩展区
         /// - Parameter text: 基本区文本
         /// - Returns: 扩展区文本
         internal mutating func Basic2Extend(_ text: String) -> String?
         
         /// 扩展区 转换 基本区
         /// - Parameter text: 扩展区文本
         /// - Returns: 基本区文本
         internal mutating func Extend2Basic(_ text: String) -> String?
         
         /// 基本区 转换 反向扩展区
         /// - Parameter text: 基本区文本
         /// - Returns: 反向扩展区文本
         internal mutating func Basic2RExtend(_ text: String) -> String?
         
         /// 反向扩展区 转换 基本区
         /// - Parameter text: 反向扩展区文本
         /// - Returns: 基本区文本
         internal mutating func RExtend2Basic(_ text: String) -> String?
         */
        
        
        // MARK: - UyghurCharUtilsDemo
        
        let source = "سالام swift"
        print("source: \(source)")
        
        guard
            let BasicSyllable = UyghurCharUtils.Shared.Convert(source, type: .BasicSyllable),
            let Basic2Extend = UyghurCharUtils.Shared.Convert(source, type: .Basic2Extend),
            let Extend2Basic = UyghurCharUtils.Shared.Convert(Basic2Extend, type: .Extend2Basic),
            let Basic2RExtend = UyghurCharUtils.Shared.Convert(source, type: .Basic2RExtend),
            let RExtend2Basic = UyghurCharUtils.Shared.Convert(Basic2RExtend, type: .RExtend2Basic)
        else {
            print("转换失败")
            return
        }
        
        print("BasicSyllable: \(BasicSyllable)")
        print("Basic2Extend: \(Basic2Extend)")
        print("Extend2Basic: \(Extend2Basic)")
        print("Basic2RExtend: \(Basic2RExtend)")
        print("RExtend2Basic: \(RExtend2Basic)")
    }
}

