from UyghurCharUtils import UyghurCharUtils

utils  = UyghurCharUtils()
source = "سالام Python";

target1 = utils.Basic2Extend(source) #基本区 转换 扩展区
target2 = utils.Extend2Basic(target1) #扩展区 转换 基本区

target3 = utils.Basic2RExtend(source) #基本区 转换 反向扩展区
target4 = utils.RExtend2Basic(target3) #反向扩展区 转换 基本区

target5 = utils.BasicSyllable(source) #音节索引

print(target1)
print(target2)
print(target3)
print(target4)
print(target5)

