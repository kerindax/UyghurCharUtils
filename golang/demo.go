package main

import (
	"fmt"
	"./UyghurCharUtils"
)

func main() {
    utils := UyghurCharUtils()
	source := "سالام go"
	utils.Basic2Extend(source)	//基本区 转换 扩展区

    target1 := utils.Basic2Extend(source)	//基本区 转换 扩展区
	target2 := utils.Extend2Basic(target1)	//扩展区 转换 基本区
	target3 := utils.Basic2RExtend(source)	//基本区 转换 反向扩展区
	target4 := utils.RExtend2Basic(target3)	//反向扩展区 转换 基本区
	target5 := utils.BasicSyllable(source)	//音节索引

	fmt.Printf("\n%s",target1)
	fmt.Printf("\n%s",target2)
	fmt.Printf("\n%s",target3)
	fmt.Printf("\n%s",target4)
	fmt.Printf("\n%s",target5)
}